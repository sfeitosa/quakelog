require "Quake_Log/version"

module QuakeLog
  class Analyser
    # Raise argument error if can't find the file
    def initialize path
      raise ArgumentError unless File.exists?( path )
      @log = File.open( path )
      @games_info = []
      @games_count = 0
    end

    def analyse
      current_game = {}
      kills = Hash.new(0)
      kills_by_means= Hash.new(0)
      total_kills = 0
      names = []
      players = {}
      @log.each_line do |line|
        if line.include? "InitGame" #Starting a Game
          if players != {}
            @games_count  += 1
            players.values.each do |player|
              names << player.player_name
              kills[player.player_name] = player.kill_count
            end
            current_game[:game_number] = @games_count
            current_game[:total_kills] = total_kills
            current_game[:players] = names
            current_game[:kills] = kills
            current_game[:kills_by_means] = kills_by_means
            @games_info << current_game
            current_game = {}
            kills = Hash.new(0)
            total_kills = 0
            players = {}
            names = []
            kills_by_means = Hash.new(0)
          end
        end

        if line.include? "ClientUserinfoChanged" # Try to get user name
          player_id = line.split[2].to_i
          player_name = line[/#{Regexp.escape("n\\")}(.*?)#{Regexp.escape("\\t")}/m, 1].strip
          if !players.keys.include? player_id
            players[player_id] =  QuakeLog::Player.new player_name
          elsif players[player_id].player_name != player_name
            players[player_id].set_player_name player_name
          end
        end

        if line.include? "Kill" # Someone died in the game
          total_kills += 1
          mean = line.split.last
          kills_by_means[mean] +=1

            if line.include? "world" #world killed someone
              player_id =  line.split[3].to_i
              players[player_id].unfrag
            else
              killer_id = line.split[2].to_i
              target_id = line.split[3].to_i
              if killer_id != target_id
                players[killer_id].frag
              else
                players[killer_id].unfrag # Player commited suicide
              end
            end
          end
        end
        @games_count  += 1
        players.values.each do |player|
          names << player.player_name
          kills[player.player_name] = player.kill_count
        end
        current_game[:game_number] = @games_count
        current_game[:total_kills] = total_kills
        current_game[:players] = names
        current_game[:kills] = kills
        current_game[:kills_by_means] = kills_by_means
        @games_info << current_game
        return @games_info
      end

    end

    class Player
      def initialize name
        @player_name = name
        @kill_count = 0
      end

      def set_player_name name
        @player_name = name
      end

      def player_name
        @player_name
      end

      def frag
        @kill_count += 1
      end

      def unfrag
       @kill_count -= 1
     end

     def kill_count
      @kill_count
    end

  end

end