module QuakeLog
  class Player
    def initialize id, name
      @id = id
      @player_name = name
      @kill_count = 0
    end

    def set_name name
      @name = name
    end

    def name
      @name
    end

    def frag
      @kill_count += 1
    end

    def unfrag
     @kill_count -= 1
   end

   def kill_count
    @kill_count
  end

end
end