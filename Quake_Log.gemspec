# -*- encoding: utf-8 -*-
require File.expand_path('../lib/Quake_Log/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["schwarzenegger"]
  gem.email         = ["schwarzeneggeral@gmail.com"]
  gem.description   = %q{Simple quake log analyser}
  gem.summary       = %q{Quake log analyser}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "Quake_Log"
  gem.require_paths = ["lib"]
  gem.version       = QuakeLog::VERSION
end
