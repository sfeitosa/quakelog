require 'Quake_Log'

describe QuakeLog do

  it "Should be able to validate  module QuakeLog" do
    (QuakeLog.is_a? Module).should eq true
  end

  it "Should be able to validate class Analyser" do
    (QuakeLog::Analyser.is_a? Class).should eq true
  end

  it "Should be able to validate class Player" do
    (QuakeLog::Player.is_a? Class).should eq true
  end

  it "Player should have name when created" do
    player = QuakeLog::Player.new "name"
    player.player_name.should eq "name"
  end

  it "Should Raise error when trying to craete a player without name" do
   expect { QuakeLog::Player.new }.to raise_error
 end

 it "Player should have kills" do
  player = QuakeLog::Player.new "name"
  player.kill_count.should eq 0
end

it "Frag should increase kill count" do
  player = QuakeLog::Player.new "name"
  player.frag
  player.kill_count.should eq 1
end

it "UnFrag should decrease kill count" do
  player = QuakeLog::Player.new "name"
  player.unfrag
  player.kill_count.should eq -1
end

it "Set player name should set a new player name" do
  player = QuakeLog::Player.new "name"
  player.set_player_name "name2"
  player.player_name.should eq "name2"
end


it "Should be able to initialize a player" do
 expect { QuakeLog::Player.new("Player") }.to_not raise_error
end


it "Should be able to initialize with file from fixtures" do
 expect { QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/log.txt') }.to_not raise_error
end

it "Should return a array for sucessfull log reading" do
  QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/log.txt') .analyse.class.should eq Array
end

it "Should be able to read names from log file" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/read_names_log.txt').analyse
  result.first[:players].should_not be_empty
end

it "Should read the following names ['Isgalamido','Lalalala' ,'Fulano' ] and avoid duplication" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/read_names_log.txt').analyse
  result.first[:players].should eq ['Isgalamido','Lalalala' ,'Fulano']
end

it "Should return a hash with kills" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
  result.first[:kills].class.should eq Hash
end

it "Isgalamido should have 2 kills" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
  result.first[:kills]["Isgalamido"].should eq 2
end

it "Should return kills_by_means as hash" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
  result.first[:kills_by_means].class.should eq Hash
end

it "MOD_ROCKET_SPLASH should have 2 kills" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
  result.first[:kills_by_means]["MOD_ROCKET_SPLASH"].should eq 2
end

it "MOD_TRIGGER_HURT should have 2 kills" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
  result.first[:kills_by_means]["MOD_TRIGGER_HURT"].should eq 1
end

it "Total Kills should be 3" do
 result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/simple_kills_log.txt').analyse
 result.first[:total_kills].should eq 3
end

it "Isgalamido should have -2 kills from killing himself" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/suicide_log.txt').analyse
  result.first[:kills]["Isgalamido"].should eq -2
end

it "Isgalamido should have -2 from dying from World " do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/world_kills_log.txt').analyse
  result.first[:kills]["Isgalamido"].should eq -2
end

it "Should a player change name the gem should not regonize this change as a new user" do
  result = QuakeLog::Analyser.new(File.dirname(__FILE__) + '/../fixtures/change_name_log.txt').analyse
  result.first[:players].should eq ["Lalalala"]
end

end