# QuakeLog

Simples parser de logs para Quake 3

##Logica

Logica do sistema é bem simples

Algotitmo vai lendo de linha a linha até cair em um dos 3 casos

  1 - Encontrou InitGame, isso quer dizer que aqui começa um jogo, guarda as variaveis do jogo anterior e reseta algumas coisas.

  2 - ClientUserinfoChanged,  algum client efetou alguma mudança, seja um player que entrou, trocou de nome ou mudou skin do personagem

  3 - Kill - Encontrar um kill significa que alguem morreu, aqui temos 3 casos:

      3.1 World - significa que o jogo matou ele subtrai uma frag do player
      3.2 Normal frag - Jogardor matou outro jogador, aumenta uma kill
      3.3 Suicidio - Jogador mata ele mesmo

Exemplo de saida

    {:game_number=>4,
    :total_kills=>105,
    :players=>["Dono da Bola", "Isgalamido", "Zeh", "Assasinu Credi"],
    :kills=>
     {"Isgalamido"=>19, "Dono da Bola"=>5, "Zeh"=>20, "Assasinu Credi"=>11},
    :kills_by_means=>
    {"MOD_TRIGGER_HURT"=>9,
     "MOD_FALLING"=>11,
     "MOD_ROCKET"=>20,
     "MOD_RAILGUN"=>8,
     "MOD_ROCKET_SPLASH"=>51,
     "MOD_MACHINEGUN"=>4,
     "MOD_SHOTGUN"=>2}}


## Instalação

Fazer Download do repositorio, ir na pasta e rodar:
   rake install

## Usage

Abrir o irb

    irb

Requisitar a gem

    require "Quake_Log"

Instanciar um objeto passando o arquivo

      log = QuakeLog::Analyser.new("log2.txt")

 Chamar a função de analisar

      log.analyse

##Testes

    rspec  spec/lib/Quake_Log_spec.rb --format documentation

    QuakeLog
    Should be able to validate  module QuakeLog
    Should be able to validate class Analyser
    Should be able to validate class Player
    Player should have name when created
    Should Raise error when trying to craete a player without name
    Player should have kills
    Frag should increase kill count
    UnFrag should decrease kill count
    Set player name should set a new player name
    Should be able to initialize a player
    Should be able to initialize with file from fixtures
    Should return a array for sucessfull log reading
    Should be able to read names from log file
    Should read the following names ['Isgalamido','Lalalala' ,'Fulano' ] and avoid duplication
    Should return a hash with kills
    Isgalamido should have 2 kills
    Should return kills_by_means as hash
    MOD_ROCKET_SPLASH should have 2 kills
    MOD_TRIGGER_HURT should have 2 kills
    Total Kills should be 3
    Isgalamido should have -2 kills from killing himself
    Isgalamido should have -2 from dying from World
    Should a player change name the gem should not regonize this change as a new user
